<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use  App\Http\Controllers\PostController;
use  App\Http\Controllers\ProjectController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/project/show',[ProjectController::class,'index']);
Route::post('/project/store',[ProjectController::class,'store']);
Route::put('/project/updateProject/{id}',[ProjectController::class,'update']);
Route::delete('/project/deleteProject/{id}',[ProjectController::class,'destroy']);
