import 'dart:convert';
class ProjectModel{
   int id;
   String name;
   String client_name;
   String code;
   ProjectModel({this.id, this.name, this.client_name, this.code});
  factory ProjectModel.fromJson(Map<String,dynamic> json){
    return ProjectModel(
      id: json["id"],name: json["name"],client_name: json["client_name"],code: json["code"]
    );
  }

  Map<String,dynamic> toJson(){
      return {
        "id":id,
        "name":name,
        "client_name":client_name,
        "code":code

      };
  }

   @override
  String toString() {
    return 'ProjectModel{id: $id, name: $name, client_name: $client_name, code: $code}';
  }
}

List<ProjectModel> postFromJson(String strJson){
  final str=json.decode(strJson);
  return List<ProjectModel>.from(
      str.map( (item){
        return ProjectModel.fromJson(item);
      })
  );
}

String postToJson(ProjectModel data){
  final dyn=data.toJson();
  return json.encode(dyn);
}
