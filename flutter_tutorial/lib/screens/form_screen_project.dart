import 'package:flutter/material.dart';
import '../models/project_model.dart';
import '../services_provider/api.dart';
var _scaffoldState=GlobalKey<ScaffoldState>();
class FormScreenProject extends StatefulWidget {

  String id;
  String name;
  String client_name;
  String code;
  bool isApiPrcess=false;

  FormScreenProject({this.id, this.name, this.client_name, this.code});

  @override
  _FormScreenProjectState createState() => _FormScreenProjectState();
}

class _FormScreenProjectState extends State<FormScreenProject> {


  TextEditingController _controllerName=TextEditingController();
  TextEditingController _controllerClientName=TextEditingController();
  TextEditingController _controllerCode=TextEditingController();

  @override
  void initState() {

    _controllerName.text=widget.name;
    _controllerClientName.text=widget.client_name;
    _controllerCode.text=widget.code;



    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldState,
      appBar: AppBar(
        title: Text("From ${widget.id}"),
      ),
      body: (widget.isApiPrcess)
          ? Center(
        child: CircularProgressIndicator(),
      )
          : Center(
        child: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  decoration: InputDecoration(hintText: "Name"),
                  keyboardType: TextInputType.text,
                  controller: _controllerName,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  decoration: InputDecoration(hintText: "Client Name"),
                  keyboardType: TextInputType.text,
                  controller: _controllerClientName,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                  decoration: InputDecoration(hintText: "Code"),
                  keyboardType: TextInputType.text,
                  controller: _controllerCode,
                ),
              ),
              widget.id == null
                  ? RaisedButton(
                child: Text("Submit"),
                onPressed: () {
                  widget.isApiPrcess = true;

                  String name =
                  _controllerName.text.toString().trim();
                  String client_name =
                  _controllerClientName.text.toString().trim();
                  //  int code=_controllerAge.text.toString().isEmpty?0:_controllerAge.text.toString() as int;
                  String code = _controllerCode.text.toString().trim();
                  if (name.isEmpty) {
                    showSnackBarMessage("Name is required");
                  } else if (client_name.isEmpty) {
                    showSnackBarMessage("Client Name is required");
                  } else if (code.isEmpty) {
                    showSnackBarMessage("Code is required");
                  } else {
                    // showSnackBarMessage("Validate Success ");

                    setState(() {
                      ProjectModel post =
                      ProjectModel(name: name, client_name: client_name, code: code);
                      createPost(post).then((response) {
                        widget.isApiPrcess = false;
                        if (response.statusCode == 200) {
                          Navigator.pop(context);
                        } else {
                          //  showSnackBarMessage("Submit data fail");
                          print("Submit data fail");
                        }
                      });

                      widget.isApiPrcess = false;
                    });
                  }
                },
              )
                  : RaisedButton(
                child: Text("Update"),
                onPressed: () {
                  widget.isApiPrcess = true;

                  String name =
                  _controllerName.text.toString().trim();
                  String client_name =
                  _controllerClientName.text.toString().trim();
                  //  int age=_controllerAge.text.toString().isEmpty?0:_controllerAge.text.toString() as int;
                  String code = _controllerCode.text.toString().trim();

                  if (name.isEmpty) {
                    showSnackBarMessage("Name is required");
                  } else if (client_name.isEmpty) {
                    showSnackBarMessage("Client Name is required");
                  } else if (code.isEmpty) {
                    showSnackBarMessage("Age is required");
                  } else {
                    // showSnackBarMessage("Validate Success ");

                    setState(() {
                      ProjectModel post =ProjectModel(id: int.parse(widget.id),name: name, client_name: client_name, code: code);
                      updatePost(post).then((response) {
                        widget.isApiPrcess = false;
                        if (response.statusCode == 200) {
                          Navigator.pop(context);
                        } else {
                          //  showSnackBarMessage("Submit data fail");
                          print("Submit data fail");
                        }
                      });

                      widget.isApiPrcess = false;
                    });
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }


  void showSnackBarMessage(String message){
    print(message);
    // Scaffold.of(context).showSnackBar(SnackBar(content: Text(message)));
    //_scaffoldState.currentState.showSnackBar(SnackBar(content: Text(message)));
  }
}
