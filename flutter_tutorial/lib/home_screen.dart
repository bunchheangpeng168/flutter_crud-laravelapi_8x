import 'package:flutter/material.dart';

import 'widgets/hr_dashbord.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView(
        gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        children: [
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: HRDashbord(),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: HRDashbord(),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: HRDashbord(),
          ),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: HRDashbord(),
          ),

        ],


      ),
    );
  }
}
