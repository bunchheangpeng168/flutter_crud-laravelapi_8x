import 'package:flutter/material.dart';
import 'pages/project_page.dart';

import 'home_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Lesson"),
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              UserAccountsDrawerHeader(

                accountName: Text("Bunchheang Peng"),
                accountEmail: Text("bunchheangpeng168@gmail.com"),


                currentAccountPicture: GestureDetector(
                  child: new CircleAvatar(
                    //   backgroundColor: Colors.grey,
                    child: Icon(
                      Icons.person,
                      color: Colors.white,
                    ),
                  ),
                ),
                //  decoration: BoxDecoration(color: Colors.red),


              ),
              InkWell(
                  onTap: () {
                    print("This is Dashboard menu");
                  },
                  child: ListTile(
                    title: Text(
                      "Dashboard",
                      style: TextStyle(color: Colors.red),
                    ),
                    leading: Icon(
                      Icons.dashboard,
                    ),
                    trailing: Icon(Icons.chevron_right),
                  )
              ),
              InkWell(
                  onTap: () {
                    print("This is HRIS menu");
                  },
                  child: ListTile(
                    title: Text(
                      "HRIS",
                      style: TextStyle(color: Colors.red),
                    ),
                    leading: Icon(
                      Icons.person_add,
                    ),
                    trailing: Icon(Icons.chevron_right),
                  )
              ),
              InkWell(
                  onTap: () {
                    print("This is QMS menu");
                  },
                  child: ListTile(
                    title: Text(
                      "QMS",
                      style: TextStyle(color: Colors.red),
                    ),
                    leading: Icon(
                      Icons.person_add,
                    ),
                    trailing: Icon(Icons.chevron_right),
                  )
              ),
              InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context){
                      return ProjectPage();
                    }  ));
                  },
                  child: ListTile(
                    title: Text(
                      "Project",
                      style: TextStyle(color: Colors.red),
                    ),
                    leading: Icon(
                      Icons.build,
                    ),
                    trailing: Icon(Icons.chevron_right),
                  )
              ),
              InkWell(
                  onTap: () {
                    print("This is HRIS menu");
                  },
                  child: ListTile(
                    title: Text(
                      "Department",
                      style: TextStyle(color: Colors.red),
                    ),
                    leading: Icon(
                      Icons.departure_board,
                    ),
                    trailing: Icon(Icons.chevron_right),
                  )
              ),       InkWell(
                  onTap: () {
                    print("This is HRIS menu");
                  },
                  child: ListTile(
                    title: Text(
                      "About",
                      style: TextStyle(color: Colors.red),
                    ),
                    leading: Icon(
                      Icons.info,
                    ),
                    trailing: Icon(Icons.chevron_right),
                  )
              ),       InkWell(
                  onTap: () {
                    print("This is HRIS menu");
                  },
                  child: ListTile(
                    title: Text(
                      "Logout",
                      style: TextStyle(color: Colors.red),
                    ),
                    leading: Icon(
                      Icons.departure_board,
                    ),
                    trailing: Icon(Icons.chevron_right),
                  )
              ),
            ],
          ),
        ),
        body: HomeScreen());
  }
}
