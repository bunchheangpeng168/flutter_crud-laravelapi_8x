import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../models/project_model.dart';
import '../services_provider/api.dart';
import '../screens/form_screen_project.dart';
var _scaffoldState=GlobalKey<ScaffoldState>();

class ProjectPage extends StatefulWidget {
  @override
  _ProjectPageState createState() => _ProjectPageState();
}

class _ProjectPageState extends State<ProjectPage> {
  var get_data_post;
  @override
  void initState() {
    // TODO: implement initState
    get_data_post= getPosts();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text("List Project"),
        actions: [
          GestureDetector(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder:(context){
                return FormScreenProject();
              } )
              );
/*  Scaffold.of(context).showSnackBar(
    SnackBar(content: Text("Text"))
  )  ;*/

//              Scaffold.of(context).showSnackBar(
//                SnackBar(
//                  content: Text('A SnackBar has been shown.'),
//                ),
//              );
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(CupertinoIcons.add),
            ),
          )
        ],
      ),body: SafeArea(
        child: FutureBuilder(
            future: get_data_post,
            builder: (context, snapshot) {
              if (snapshot.hasError) {
                return Center(child: Text(snapshot.error.toString()));
              } else if (snapshot.connectionState == ConnectionState.done) {
                var response = snapshot.data as List<ProjectModel>;

                print(response);
                return ListView.builder(
                  itemCount: response.length,
                  itemBuilder: (context, index) {
                    var postItem = response[index];
                    return Card(


                      child: ListTile(
                        leading: Icon(Icons.person),
                        title: Text(postItem.name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0)
                        ),
                        subtitle:     Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Email: "+postItem.client_name,style: TextStyle(color: Colors.blueAccent),),
                            Text(" Age: "+postItem.code.toString(),style: TextStyle(color: Colors.blueAccent),),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [

                                InkWell(onTap: (){
                                  print("ID Form: "+postItem.id.toString());
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              FormScreenProject(
                                                id: postItem.id
                                                    .toString(),
                                                name: postItem.name,
                                                client_name: postItem.client_name,
                                                code: postItem.code,
                                              )
                                      )
                                  );
                                }, child: Text("Edit",
                                  style:TextStyle(color: Colors.blueAccent),
                                )
                                ),SizedBox(width: 20,)
                                , InkWell(onTap:  (){
                                  showDialog(
                                      context: context,
                                      builder: (context){
                                        return AlertDialog(
                                          title: Text("Delete data"),
                                          content: Text("Are you sure?"),
                                          actions: [
                                            FlatButton(
                                                onPressed: (){
                                                  deletePost(postItem.id).then((response){
                                                                setState(() {
                                                                  get_data_post=    getPosts();
                                                                });
                                                  });
                                                },
                                                child:Text("Yes")
                                            ),
                                            FlatButton(
                                                onPressed: (){
                                                  Navigator.pop(context);
                                                },
                                                child:Text("No")
                                            )
                                          ],
                                        );
                                      }
                                  );

                                }, child: Text("Delete",
                                  style:TextStyle(color: Colors.redAccent),
                                )
                                )
                              ],
                            ),

                          ],
                        ),
                        trailing: InkWell(
                          onTap: (){

//                            Navigator.push(
//                              context,
//                              MaterialPageRoute(builder: (context) => PageDetail(id: postItem.id  ,)),
//                            );


                          },
                          child: Icon(Icons.forward),
                        ),

                      ),


                    );
                  },
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            })
    ),
    );
  }
}
