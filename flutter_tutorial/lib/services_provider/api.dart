import '../models/project_model.dart';
import 'package:http/http.dart' as http;

String baseUrl="http://127.0.0.1:8000/api/project";
//String baseUrl="http://localhost:51818";
Future<List<ProjectModel>>getPosts() async{
  final response =await http.get("$baseUrl/show");
  return postFromJson(response.body);
}
Future<http.Response> createPost(ProjectModel post) async{
  final response =await http.post(
      "$baseUrl/store",
      headers: {
        "Content-Type":"application/json"
      },
      body: postToJson(post)
  );
  return response;
}
Future<http.Response>updatePost(ProjectModel post) async{
  print("hello");

  final response=await http.put(
      "$baseUrl/updateProject/${post.id}",
      headers: {
        "Content-Type":"application/json"
      },
      body: postToJson(post)
  );

  return response;
}

Future<http.Response>deletePost(int id) async{
  final response =await http.delete("$baseUrl/deleteProject/$id");
  return response;
}